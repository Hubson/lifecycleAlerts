import React from 'react';
import ReactDOM from 'react-dom';

export class Flashy extends React.Component {
  componentWillMount() {
    alert('Pierwszy alert!');
  }
	componentDidMount() {
        alert('Drugi alert!');
  }
  render() {

    alert('Alert w renderingu!');


    return (
      <h1 style={{ color: this.props.color }}>
        Zmienilem kolor po alercie
      </h1>
    );
  }
}

ReactDOM.render(
  <Flashy color='red' />,
  document.getElementById('app')
);

setTimeout(() => {
  ReactDOM.render(
    <Flashy color='green' />,
    document.getElementById('app')
  );
}, 2000);